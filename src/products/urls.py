from django.urls import path

from .views import (ProductListView, ProductDetailView,
                    ProductFeaturedListView, ProductFeaturedDetailView,
                    ProductDetailSlugView)

urlpatterns = [
    path('', ProductListView.as_view()),
    path('product/<int:pk>/', ProductDetailView.as_view()),
    path('featured/', ProductFeaturedListView.as_view()),
    path('featured/<int:pk>/', ProductFeaturedDetailView.as_view()),
    path('product/<slug:slug>/', ProductDetailSlugView.as_view(), name='detail'),
]
