
from django.contrib import admin
from django.conf import settings
from django.urls import path, include
from django.contrib.auth.views import LogoutView
from django.conf.urls.static import static

from address.views import checkout_address_create_view, checkout_address_reuse_view
from accounts.views import login_page, guest_register_view
from carts.views import cart_detail_api_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('products.urls')),
    path('search/', include('search.urls', namespace='search')),
    path('', include('accounts.urls', namespace='accounts')),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('api/cart/', cart_detail_api_view, name='api-cart'),
    path('cart/', include('carts.urls', namespace='cart')),
    path('login/', login_page, name='login'),
    path('checkout/address/create/', checkout_address_create_view,
         name='checkout_address_create'),
    path('checkout/address/reuse/', checkout_address_reuse_view,
         name='checkout_address_reuse'),
    path('register/guest/', guest_register_view, name='guest_register'),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
